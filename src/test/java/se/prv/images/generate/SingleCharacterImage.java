package se.prv.images.generate;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.swing.JFrame; 
import javax.swing.JPanel; 

public class SingleCharacterImage {
	
	public final static int imageWidth = 28;
	public final static int imageHeight = 28;

	public static ImageBundle generateSmallStraightImage(char c, Font font) throws IOException {
		ActualInsets actualInsets = ActualInsets.getActualInsets(c, font);
		ImageBundle out = new ImageBundle(actualInsets); 
		
		System.out.println(""+actualInsets);
		System.out.println(""+out);
		
		BufferedImage bufferedImage = new BufferedImage(
				out.width, 
				out.height+1, 
				BufferedImage.TYPE_BYTE_GRAY);
		Graphics2D g2d = bufferedImage.createGraphics();
		RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g2d.setRenderingHints(rh);
		g2d.setPaint(Color.WHITE);
		g2d.fillRect(0, 0, out.width, out.height+1);
		g2d.setColor(Color.BLACK);
		g2d.setFont(font);

		g2d.drawChars(new char[] { c }, 0, 1, out.drawPosHor, out.drawPosVer);
		g2d.dispose();
		
		out.image = bufferedImage;

		return out;
	} 
	
	public static BufferedImage generateStandardSizedTiltedImage(ImageBundle imageBundle, int posHor, int posVer, double angle) {
		BufferedImage bufferedImage = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_BYTE_GRAY);
		Graphics2D g2d = bufferedImage.createGraphics();

		RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

		g2d.setRenderingHints(rh);
		g2d.setPaint(Color.WHITE);
		g2d.fillRect(0, 0, imageWidth, imageHeight);

		int locationX = imageBundle.width / 2;
		int locationY = imageBundle.height / 2;
		AffineTransform tx = AffineTransform.getRotateInstance(angle, locationX, locationY);
		AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);

		// Drawing the rotated image at the required drawing locations
		g2d.drawImage(op.filter(imageBundle.image, null), posHor, posVer, null);

		return bufferedImage;
	}
	
	public static void main(String [] args) throws IOException {
		char c = 'g';// X{}[]

		Font font = new Font("TimesRoman", Font.PLAIN, 18);
		ImageBundle smallStraightImageBundle = generateSmallStraightImage(c, font);

		JFrame rawImageFrame = new JFrame();
		rawImageFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		rawImageFrame.setVisible(true);
		rawImageFrame.setLocation(240, 30);
		rawImageFrame.getContentPane().add(new SomeJPanel(smallStraightImageBundle.actualInsets.image));
		rawImageFrame.setSize(imageWidth,  imageHeight+22);
		rawImageFrame.invalidate();
		rawImageFrame.validate();

		JFrame straightImageFrame = new JFrame();
		straightImageFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		straightImageFrame.setVisible(true);
		straightImageFrame.setLocation(40, 30);
		straightImageFrame.getContentPane().add(new SomeJPanel(smallStraightImageBundle.image));
		straightImageFrame.setSize(imageWidth,  imageHeight+22);
		straightImageFrame.invalidate();
		straightImageFrame.validate();

		Image smallTiltedImage = generateStandardSizedTiltedImage(smallStraightImageBundle, 3, 5, 0.1);
		
		JFrame titledImageFrame = new JFrame();
		titledImageFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		titledImageFrame.setVisible(true);
		titledImageFrame.setLocation(140, 30);
		titledImageFrame.getContentPane().add(new SomeJPanel(smallTiltedImage));
		titledImageFrame.setSize(imageWidth,  imageHeight+22);
		titledImageFrame.invalidate();
		titledImageFrame.validate();
	}
	
	public static class SomeJPanel extends JPanel {
		
		private Image image;
		
		public SomeJPanel(Image image) {
			this.image = image;
		}
		
	    @Override
	    protected void paintComponent(Graphics g) {
	        super.paintComponent(g);
	        g.drawImage(image, 0, 0, this);             
	    }

	}

}

// To pixels
// https://www.researchgate.net/post/How_to_convert_grayscale_image_to_binary_image_in_java

// Rotate
// https://stackoverflow.com/questions/8639567/java-rotating-images

