package se.prv.images.generate;

public class TrainingImageParameters {
	
	public ImageBundle bundle;
	public int posHor;
	public int posVer;
	public double angle;
	
	public TrainingImageParameters(ImageBundle bundle, int posHor, int posVer, double angle) {
		this.bundle = bundle;
		this.posHor = posHor;
		this.posVer = posVer;
		this.angle = angle;
	}

}
