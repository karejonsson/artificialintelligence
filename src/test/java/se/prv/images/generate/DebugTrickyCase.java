package se.prv.images.generate;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.JFrame;

import se.prv.images.generate.SingleCharacterImage.SomeJPanel;

public class DebugTrickyCase {
	
	public static void main(String[] args) throws IOException {
		char c = '}';
		int size = 20;
		String fontname = "TimesRoman";
		
		Font font = new Font(fontname, Font.BOLD, size);
		ImageBundle smallStraightImageBundle = SingleCharacterImage.generateSmallStraightImage(c, font);

		JFrame straightImageFrame = new JFrame("ORIG");
		straightImageFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		straightImageFrame.setVisible(true);
		straightImageFrame.setLocation(240, 130);
		straightImageFrame.getContentPane().add(new SomeJPanel(smallStraightImageBundle.image));
		straightImageFrame.setSize(SingleCharacterImage.imageWidth,  SingleCharacterImage.imageHeight);
		straightImageFrame.invalidate();
		straightImageFrame.validate();
	}
	
}
