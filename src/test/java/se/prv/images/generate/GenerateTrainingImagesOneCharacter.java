package se.prv.images.generate;

import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import general.reuse.swingutils.illustration.HeatMap;
import general.reuse.swingutils.paneling.PanelWithClosePlaces;

public class GenerateTrainingImagesOneCharacter { 

	public static void main(String[] args) throws IOException {
		char c = 'm';
		//int size = 18;
		String fontname = "TimesRoman";
		int fontstyle = Font.PLAIN;
		
		List<TrainingImageParameters> allCombinations = new ArrayList<TrainingImageParameters>();
		for(int size = 18 ; size <= 22 ; size++) {
			ImageBundle bundle = GenerateTrainingImages.getBundle(c, size, "TimesRoman", Font.PLAIN);
			List<TrainingImageParameters> combinations = GenerateTrainingImages.getCombinations(bundle);
			allCombinations.addAll(combinations);
		}
		Collections.shuffle(allCombinations);

		System.out.println("Antal "+allCombinations.size());
		double hh = Math.sqrt(allCombinations.size() / 2.0);
		double ww = allCombinations.size() / hh;
		
		int h = 20;
		int w = 50;
		
		System.out.println("h = "+h+", w = "+w+", h*w = "+(h*w));
		
		PanelWithClosePlaces grid = new PanelWithClosePlaces(h, w);
		int ctr = 0;
		for(int y = 0 ; y < h ; y++) {
			for(int x = 0 ; x < w ; x++) {
				if(allCombinations.size() > ctr) {
					TrainingImageParameters parameters = allCombinations.get(ctr++);
					BufferedImage image = SingleCharacterImage.generateStandardSizedTiltedImage(parameters.bundle, parameters.posHor, parameters.posVer, parameters.angle);
					JPanel p = new SingleCharacterImage.SomeJPanel(image);
					p.setToolTipText(""+c);
					//double[] map = Reconstruct28By28Inputs.reconstruct(image);
					//HeatMap p = Reconstruct28By28Inputs.getHeatMap(map);
					grid.setPanel(y, x, p);
				}
			}
		}
		
		JFrame f1 = new JFrame();
		f1.setSize(1800, 800);
		f1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f1.setVisible(true);
		f1.setLocation(0, 0);
		f1.getContentPane().add(grid);
		f1.invalidate();
		f1.validate();

	}
}
