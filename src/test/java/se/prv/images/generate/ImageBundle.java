package se.prv.images.generate;

import java.awt.image.BufferedImage;

public class ImageBundle {

	public BufferedImage image;
	public int width;
	public int height;
	public int drawPosVer;
	public int drawPosHor;
	public ActualInsets actualInsets;

	public String toString() {
		return "ImageBundle{width="+width+", height="+height+", drawPosVer="+drawPosVer+", drawPosHor="+drawPosHor+", actualInsets="+actualInsets+"}";
	}
	
	public static final String hasLowPart = "{}[]gpqj";
	public static final String mustBeHigh = "\"'`";
	public static final String mustBeLow = "_";
	public static final String mustBeCenter = "-=";

	public ImageBundle(ActualInsets actualInsets) { 
		this.actualInsets = actualInsets;
		if(mustBeLow.indexOf(actualInsets.c) != -1) {
			height = (int) (actualInsets.font.getSize()*0.7);
			width = (int) (actualInsets.font.getSize()*0.7);
			drawPosVer = height-1;
			drawPosHor = -actualInsets.firstHor;
			return;
		}
		if(mustBeHigh.indexOf(actualInsets.c) != -1) {
			height = (int) (actualInsets.font.getSize()*0.7);
			width = (int) (actualInsets.font.getSize()*0.7);
			drawPosVer = height;
			drawPosHor = -actualInsets.firstHor;
			return;
		}
		if(mustBeCenter.indexOf(actualInsets.c) != -1) {
			height = actualInsets.font.getSize()/2;
			width = actualInsets.font.getSize()/2;
			drawPosVer = height;
			drawPosHor = 0;
			return;
		}
		if(hasLowPart.indexOf(actualInsets.c) != -1) {
			int corr = Math.min(3, actualInsets.firstVer)+actualInsets.lastVer-actualInsets.font.getSize();
			corr = Math.max(0, corr) + 2;
			if(actualInsets.lastVer % 2 == 0) {
				corr++;
			}
			System.out.println("---Corr = "+corr);
			height = actualInsets.lastVer - actualInsets.firstVer + 1;
			width = actualInsets.lastHor - actualInsets.firstHor + 1;
			drawPosVer = height-corr;
			drawPosHor = -actualInsets.firstHor;
			return;
		}
		height = actualInsets.lastVer - actualInsets.firstVer + 1;
		width = actualInsets.lastHor - actualInsets.firstHor + 1;
		drawPosVer = height;
		drawPosHor = -actualInsets.firstHor;
	}
	
}
