package se.prv.images.generate;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

public class ActualInsets {
	
	public int firstHor = Integer.MAX_VALUE;
	public int lastHor = Integer.MIN_VALUE;
	public int firstVer = Integer.MAX_VALUE;
	public int lastVer = Integer.MIN_VALUE;
	public char c;
	public Font font;
	public BufferedImage image = null;
	
	public String toString() {
		return "ActualInsets{firstHor="+firstHor+", lastHor="+lastHor+", firstVer="+firstVer+", lastVer="+lastVer+", c="+c+", font="+font+"}";
	}
	
	private ActualInsets(char c, Font font) {
		this.c = c;
		this.font = font;
	}
	
	public static final int ex = 5;

	public static ActualInsets getActualInsets(char c, Font font) {
		int fontSize = font.getSize();
		BufferedImage image = new BufferedImage(fontSize+ex, fontSize+ex, BufferedImage.TYPE_BYTE_GRAY);
		Graphics2D g2d = image.createGraphics();
		RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g2d.setRenderingHints(rh);
		g2d.setPaint(Color.WHITE);
		g2d.fillRect(0, 0, fontSize+ex, fontSize+ex);
		g2d.setColor(Color.BLACK);
		g2d.setFont(font);
		FontMetrics metrics = g2d.getFontMetrics(font);
		Rectangle2D rectangle = metrics.getStringBounds(new String(new char[] { c }), 0, 1, null);
		int heightChar = (int) (-rectangle.getY());

		g2d.drawChars(new char[] { c }, 0, 1, 0, heightChar);
		//g2d.dispose();
		ActualInsets out = new ActualInsets(c, font);
		out.image = image;
		
		WritableRaster raster = image.getRaster();
		
		for(int y = 0 ; y < image.getHeight() ; y++) {
			for(int x = 0 ; x < image.getWidth() ; x++) {
                int[] p = new int[4];
                raster.getPixel(x, y, p);
                //System.out.println("x "+x+", y "+y+" -> "+Arrays.toString(p));
                if(p[0] != 255) {
                	//System.out.println("Fläck x "+x+", y "+y);
                	out.firstHor = Math.min(out.firstHor, x);
                	out.lastHor = Math.max(out.lastHor, x);

                	out.firstVer = Math.min(out.firstVer, y);
                	out.lastVer = Math.max(out.lastVer, y);
                }
			}
		}

		return out;
	}
	
	public static void main(String args[]) {
		Font font = new Font("TimesRoman", Font.BOLD, 18);
		System.out.println(""+getActualInsets('{', font));
	}
	
}
