package se.prv.images.generate;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JPanel;

import general.reuse.swingutils.colors.VariousGenerators;
import general.reuse.swingutils.illustration.HeatMap;
import general.reuse.swingutils.illustration.HeatSpot;
import se.prv.ai.neuralnet.example.digit.DigitImageLoadingService;
import se.prv.images.generate.SingleCharacterImage.SomeJPanel;

public class Reconstruct28By28Inputs {

	public static double[] reconstruct(BufferedImage image) {
		double[] out = new double[image.getWidth() * image.getHeight()];
		int pos = 0;
		
		WritableRaster raster = image.getRaster();
		
		for(int y = 0 ; y < image.getHeight() ; y++) {
			for(int x = 0 ; x < image.getWidth() ; x++) {
                int[] p = new int[4];
                raster.getPixel(y, x, p);
                if(!(p[0] == 255)) {
                //if(!(p[0] % 0x0F == )) {
                	out[pos] = 1.0;
                }
                //System.out.println("X "+x+", Y "+y+" = "+out[pos]+", p "+Arrays.toString(p));
                pos++;
			}
		}
		
		return out;
	}
	
	public static HeatMap getHeatMap(double[] input) {
		HeatSpot[][] picture = new HeatSpot[DigitImageLoadingService.ROWS][DigitImageLoadingService.COLUMNS];
		for(int i = 0 ; i < input.length ; i++) {
			int ver = i / DigitImageLoadingService.COLUMNS;
			int hor = i % DigitImageLoadingService.COLUMNS;
			picture[ver][hor] = getHeatSpot(input[i]);
		}
		VariousGenerators vg = new VariousGenerators(Color.red, Color.blue);
		HeatMap hm = new HeatMap(
				picture, 
				true, 
				vg.getColors(100));
		return hm;
	}

	private static HeatSpot getHeatSpot(final double d) {
		return new HeatSpot() {
			@Override
			public double getHeat() {
				return d;
			}
		};
	}
	
	public static void main(String[] args) throws IOException {
		char c = '}';
		int size = 20;
		double angle = 0.05;
		int posHor = 12;
		int posVer = 0;
		String fontname = "TimesRoman";
		
		Font font = new Font(fontname, Font.BOLD, size);
		ImageBundle smallStraightImageBundle = SingleCharacterImage.generateSmallStraightImage(c, font);

		JFrame straightImageFrame = new JFrame("ORIG");
		straightImageFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		straightImageFrame.setVisible(true);
		straightImageFrame.setLocation(240, 130);
		straightImageFrame.getContentPane().add(new SomeJPanel(smallStraightImageBundle.image));
		straightImageFrame.setSize(SingleCharacterImage.imageWidth,  SingleCharacterImage.imageHeight);
		straightImageFrame.invalidate();
		straightImageFrame.validate();

		BufferedImage smallTiltedBoldImage = SingleCharacterImage.generateStandardSizedTiltedImage(smallStraightImageBundle, posHor, posVer, angle);

		System.out.println("BOLD: HF "+smallStraightImageBundle.height+", HC "+smallStraightImageBundle.height+", W: "+smallStraightImageBundle.width);
		double[] mapBold = reconstruct(smallTiltedBoldImage);
		
		JFrame reconstructedBoldImageFrame = new JFrame("BOLD");
		reconstructedBoldImageFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		reconstructedBoldImageFrame.setVisible(true);
		reconstructedBoldImageFrame.setLocation(140, 30);
		reconstructedBoldImageFrame.getContentPane().add(getHeatMap(mapBold));
		reconstructedBoldImageFrame.setSize(SingleCharacterImage.imageWidth+22,  SingleCharacterImage.imageHeight+22);
		reconstructedBoldImageFrame.invalidate();
		reconstructedBoldImageFrame.validate();
		
		JFrame titledImageFrame = new JFrame("ORIG 28*28");
		titledImageFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		titledImageFrame.setVisible(true);
		titledImageFrame.setLocation(140, 130);
		titledImageFrame.getContentPane().add(new SomeJPanel(smallTiltedBoldImage));
		titledImageFrame.setSize(SingleCharacterImage.imageWidth,  SingleCharacterImage.imageHeight);
		titledImageFrame.invalidate();
		titledImageFrame.validate();

		font = new Font(fontname, Font.PLAIN, size);
		smallStraightImageBundle = SingleCharacterImage.generateSmallStraightImage(c, font);
		BufferedImage smallTiltedPlainImage = SingleCharacterImage.generateStandardSizedTiltedImage(smallStraightImageBundle, posHor, posVer, angle);

		System.out.println("PLAIN: HF "+smallStraightImageBundle.height+", HC "+smallStraightImageBundle.height+", W: "+smallStraightImageBundle.width);
		double[] mapPlain = reconstruct(smallTiltedPlainImage);

		JFrame reconstructedPlainImageFrame = new JFrame("PLAIN");
		reconstructedPlainImageFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		reconstructedPlainImageFrame.setVisible(true);
		reconstructedPlainImageFrame.setLocation(140, 230);
		reconstructedPlainImageFrame.getContentPane().add(getHeatMap(mapPlain));
		reconstructedPlainImageFrame.setSize(SingleCharacterImage.imageWidth+22,  SingleCharacterImage.imageHeight+22);
		reconstructedPlainImageFrame.invalidate();
		reconstructedPlainImageFrame.validate();
		
	}

}
