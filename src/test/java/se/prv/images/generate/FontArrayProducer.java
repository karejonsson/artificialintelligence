package se.prv.images.generate;

import java.awt.Font;

public class FontArrayProducer {

	public static String[] getFontnames() {
		return new String[] { "TimesRoman", "Courier" }; 
	}

	public static int[] getFontstyles() {
		return new int[] { Font.BOLD, Font.ITALIC, Font.PLAIN }; 
	}

}
