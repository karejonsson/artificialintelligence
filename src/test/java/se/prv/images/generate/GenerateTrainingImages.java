package se.prv.images.generate;

import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import general.reuse.swingutils.illustration.HeatMap;
import general.reuse.swingutils.paneling.PanelWithClosePlaces;

public class GenerateTrainingImages {
	
	public static double[] getImage(char c, int size, String fontname, int fontstyle, int posHor, int posVer, double angle) throws IOException {
		Font font = new Font(fontname, fontstyle, size);
		ImageBundle bundle = SingleCharacterImage.generateSmallStraightImage(c, font);
		BufferedImage image = SingleCharacterImage.generateStandardSizedTiltedImage(bundle, posHor, posVer, angle);
		return Reconstruct28By28Inputs.reconstruct(image);
	}
	
	public static ImageBundle getBundle(char c, int size, String fontname, int fontstyle) throws IOException {
		return SingleCharacterImage.generateSmallStraightImage(c, new Font(fontname, fontstyle, size));
	}
	
	public static ImageBundle getBundle(char c, Font font) throws IOException {
		return SingleCharacterImage.generateSmallStraightImage(c, font);
	}
	
	public static List<TrainingImageParameters> getCombinations(ImageBundle bundle) {
		List<TrainingImageParameters> out = new ArrayList<TrainingImageParameters>();
		for(int posHor = 0 ; posHor < SingleCharacterImage.imageWidth - bundle.width ; posHor++) {
			for(int posVer = 0 ; posVer < SingleCharacterImage.imageHeight - bundle.height ; posVer++) {
				for(double angle = -0.1 ; angle <= 0.1 ; angle += 0.05) {
					out.add(new TrainingImageParameters(bundle, posHor, posVer, angle));
				}
			}
		}
		return out;
	}
	
	public static final String allChars = "abcdefghijklmonpqrstuvwxyzåäöABCDEFGHIJKLMNOPQRSTUVWXYZÅÄÖ0123456789!\"#¤%&/\\()=?£@$€¥{[]}'`.,;:<>|-_";

	public static void main(String[] args) throws IOException {
		String fontnames[] = FontArrayProducer.getFontnames();
		int fontstyles[] = FontArrayProducer.getFontstyles();
		List<TrainingImageParameters> allCombinations = new ArrayList<TrainingImageParameters>();
		String chars = allChars;//ImageBundle.mustBeLow;
		for(String fontname : fontnames) {
			for(int fontstyle : fontstyles) {
				for(int i = 0 ; i < chars.length() ; i++) {
					char c = chars.charAt(i);
					for(int size = 20 ; size <= 28 ; size++) {
						ImageBundle bundle = getBundle(c, size, fontname, fontstyle);
						List<TrainingImageParameters> combinations = getCombinations(bundle);
						allCombinations.addAll(combinations);
					}
				}
			}
		}

		System.out.println("Generated "+allCombinations.size()+" training datas");
		
		Collections.shuffle(allCombinations);
		int h = 25;
		int w = 50;
		
		System.out.println("h = "+h+", w = "+w+", h*w = "+(h*w));
		
		PanelWithClosePlaces grid = new PanelWithClosePlaces(h, w);
		int ctr = 0;
		for(int y = 0 ; y < h ; y++) {
			for(int x = 0 ; x < w ; x++) {
				if(allCombinations.size() > ctr) {
					TrainingImageParameters parameters = allCombinations.get(ctr++);
					BufferedImage image = SingleCharacterImage.generateStandardSizedTiltedImage(parameters.bundle, parameters.posHor, parameters.posVer, parameters.angle);
					//JPanel p = new SingleCharacterImage.SomeJPanel(image);
					double[] map = Reconstruct28By28Inputs.reconstruct(image);
					HeatMap p = Reconstruct28By28Inputs.getHeatMap(map);
					p.setToolTipText(""+parameters.bundle.actualInsets.c);
					grid.setPanel(y, x, p);
				}
			}
		}
		
		JFrame f1 = new JFrame();
		f1.setSize(1800, 800);
		f1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f1.setVisible(true);
		f1.setLocation(0, 0);
		f1.getContentPane().add(grid);
		f1.invalidate();
		f1.validate();

	}

	/*
	public static void main(String[] args) throws IOException {
		char c = 'm';
		int size = 18;
		String fontname = "TimesRoman";
		int fontstyle = Font.PLAIN;
		
		ImageBundle bundle = getBundle(c, size, fontname, fontstyle);
		List<TrainingImageParameters> combinations = getCombinations(bundle);
		
		System.out.println("Antal "+combinations.size());
		double hh = Math.sqrt(combinations.size() / 2.0);
		double ww = combinations.size() / hh;
		
		//int h = 10;
		//int w = 20;
		
		int h = 1+(int) (hh+0.0);
		int w = 1+(int) (ww+0.0);
		
		System.out.println("h = "+h+", w = "+w+", h*w = "+(h*w));
		
		PanelWithClosePlaces grid = new PanelWithClosePlaces(h, w);
		int ctr = 0;
		for(int y = 0 ; y < h ; y++) {
			for(int x = 0 ; x < w ; x++) {
				if(combinations.size() > ctr) {
					TrainingImageParameters parameters = combinations.get(ctr++);
					BufferedImage image = SingleCharacterImage.generateStandardSizedTiltedImage(parameters.bundle, parameters.posHor, parameters.posVer, parameters.angle);
					//JPanel p = new SingleCharacterImage.SomeJPanel(image);
					double[] map = Reconstruct28By28Inputs.reconstruct(image);
					HeatMap p = Reconstruct28By28Inputs.getHeatMap(map);
					grid.setPanel(y, x, p);
				}
			}
		}
		
		JFrame f1 = new JFrame();
		f1.setSize(1800, 800);
		f1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f1.setVisible(true);
		f1.setLocation(0, 0);
		f1.getContentPane().add(grid);
		f1.invalidate();
		f1.validate();

	}
	*/
}
