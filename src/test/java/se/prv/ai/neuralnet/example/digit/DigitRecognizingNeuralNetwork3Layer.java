package se.prv.ai.neuralnet.example.digit;

import se.prv.ai.neuralnet.activators.LinearActivationStrategy;
import se.prv.ai.neuralnet.activators.SigmoidActivationStrategy;
import se.prv.ai.neuralnet.core.Layer;
import se.prv.ai.neuralnet.core.NeuralNetwork;
import se.prv.ai.neuralnet.core.Neuron;
import se.prv.ai.neuralnet.gui.BackpropagatorControlPanel;
import se.prv.ai.neuralnet.gui.BackpropagatorScorePanel;
import se.prv.ai.neuralnet.gui.HeatGivingSynapseFactory;
import se.prv.ai.neuralnet.gui.HeatMapPopupListener;
import se.prv.ai.neuralnet.training.Backpropagator;
import se.prv.ai.neuralnet.training.TrainingData;

import java.awt.Color;
import java.io.IOException;

import javax.swing.JFrame;

import general.reuse.swingutils.colors.VariousGenerators;
import general.reuse.swingutils.illustration.HeatMap;
import general.reuse.swingutils.illustration.HeatSpot;
import general.reuse.swingutils.paneling.PanelWithClosePlaces;

/**
 * Created by IntelliJ IDEA.
 * User: vivin
 * Date: 11/11/11
 * Time: 10:02 AM
 */
public class DigitRecognizingNeuralNetwork3Layer {

	public static void main(String[] args) throws IOException {

		DigitImageLoadingService trainingService = new DigitImageLoadingService("/train/train-labels-idx1-ubyte.dat", "/train/train-images-idx3-ubyte.dat");

		final NeuralNetwork neuralNetwork = new NeuralNetwork("Digit Recognizing Neural Network by Kåre");

		Neuron inputBias = new Neuron(new LinearActivationStrategy());
		inputBias.setOutput(1);

		Layer inputLayer = new Layer(inputBias);

		for(int i = 0; i < DigitImageLoadingService.ROWS * DigitImageLoadingService.COLUMNS; i++) {
			Neuron neuron = new Neuron(new SigmoidActivationStrategy());
			neuron.setOutput(0);
			inputLayer.addNeuron(neuron);
		}

		Neuron hiddenBias = new Neuron(new LinearActivationStrategy());
		hiddenBias.setOutput(1);

		HeatGivingSynapseFactory sf1 = new HeatGivingSynapseFactory("hidden 1");
		Layer hiddenLayer1 = new Layer(sf1, inputLayer, hiddenBias); 

		long numberOfHiddenNeuronsLayer1 = 40;

		for(int i = 0; i < numberOfHiddenNeuronsLayer1; i++) {
			Neuron neuron = new Neuron(new SigmoidActivationStrategy());
			neuron.setOutput(0);
			hiddenLayer1.addNeuron(neuron);
		}

		HeatGivingSynapseFactory sf2 = new HeatGivingSynapseFactory("hidden 2");
		Layer hiddenLayer2 = new Layer(sf2, hiddenLayer1, hiddenBias);

		long numberOfHiddenNeuronsLayer2 = 40;

		for(int i = 0; i < numberOfHiddenNeuronsLayer2; i++) {
			Neuron neuron = new Neuron(new SigmoidActivationStrategy());
			neuron.setOutput(0);
			hiddenLayer2.addNeuron(neuron);
		}

		HeatGivingSynapseFactory sfOutput = new HeatGivingSynapseFactory("output");
		Layer outputLayer = new Layer(sfOutput, hiddenLayer2);

		//10 output neurons - 1 for each digit
		for(int i = 0; i < 10; i++) {
			Neuron neuron = new Neuron(new SigmoidActivationStrategy());
			neuron.setOutput(0);
			outputLayer.addNeuron(neuron);
		}

		neuralNetwork.addLayer(inputLayer);
		neuralNetwork.addLayer(hiddenLayer1);
		neuralNetwork.addLayer(hiddenLayer2);
		neuralNetwork.addLayer(outputLayer);

		final DigitTrainingDataGeneratorStatefulRandom trainingDataGenerator = new DigitTrainingDataGeneratorStatefulRandom(trainingService.loadDigitImages(), 600);
		final Backpropagator backpropagator = new Backpropagator(neuralNetwork, 0.1, 0.9, 0);

		Runnable trainer = new Runnable() {
			public void run() {
				backpropagator.train(trainingDataGenerator, 0.005);
				stop = true;
			}
		};

		Runnable tester = new Runnable() {
			public void run() {
				DigitTrainingDataGeneratorStatefulRandom testDataGenerator;
				try {
					DigitImageLoadingService testService = new DigitImageLoadingService("/test/t10k-labels-idx1-ubyte.dat", "/test/t10k-images-idx3-ubyte.dat");
					testDataGenerator = new DigitTrainingDataGeneratorStatefulRandom(testService.loadDigitImages(), 10);
				} catch (IOException e) {
					e.printStackTrace();
					return;
				}
				TrainingData testData = testDataGenerator.getData(testDataGenerator.getDataSize());
				
				int ctr = 0;
				double[][] inputs = testData.getInputs();

				for(int i = 0; i < inputs.length; i++) {
					double[] input = inputs[i];
					double[] output = testData.getOutputs()[i];

					int digit = 0;
					boolean found = false;
					while(digit < 10 && !found) {
						found = (output[digit] == 1);
						digit++;
					}

					neuralNetwork.setInputs(input);
					double[] receivedOutput = neuralNetwork.getOutput();

					double max = receivedOutput[0];
					double recognizedDigit = 0;
					for(int j = 0; j < receivedOutput.length; j++) {
						if(receivedOutput[j] > max) {
							max = receivedOutput[j];
							recognizedDigit = j;
						}
					}

					String attention = " <--- Wrong";
					if(((int) recognizedDigit) == (digit - 1)) {
						ctr++;
						attention = "";
					}
					System.out.println("Recognized " + (digit - 1) + " as " + recognizedDigit + ". Corresponding output value was " + max+attention);
				}
				System.out.println("Correct "+ctr+" of "+inputs.length);
				System.out.println("-------------------------------------------------");
			}

		};
		new Thread(trainer).start();
		PanelWithClosePlaces p = new PanelWithClosePlaces(2, 3);

		HeatSpot[][] matrix1 = sf1.getHeatGrid();
		VariousGenerators vg1 = new VariousGenerators(Color.red, Color.blue);
		HeatMap hm1 = new HeatMap(
				matrix1, 
				true, 
				vg1.getColors(100));
		hm1.addMouseListener(new HeatMapPopupListener(hm1, sf1));
		hm1.setToolTipText("Y "+matrix1.length+", X "+matrix1[0].length);
		p.setPanel(0, 0, hm1);

		HeatSpot[][] matrix2 = sf2.getHeatGrid();
		VariousGenerators vg2 = new VariousGenerators(Color.red, Color.blue);
		HeatMap hm2 = new HeatMap(
				matrix2, 
				true, 
				vg2.getColors(100)); 
		hm2.addMouseListener(new HeatMapPopupListener(hm2, sf2));
		hm2.setToolTipText("Y "+matrix2.length+", X "+matrix2[0].length);
		p.setPanel(0, 1, hm2);

		HeatSpot[][] matrixO = sfOutput.getHeatGrid();
		VariousGenerators vgO = new VariousGenerators(Color.red, Color.blue);
		HeatMap hmO = new HeatMap(
				matrixO, 
				true, 
				vgO.getColors(100)); 
		hmO.addMouseListener(new HeatMapPopupListener(hmO, sfOutput));
		hmO.setToolTipText("Y "+matrixO.length+", X "+matrixO[0].length);
		p.setPanel(0, 2, hmO);

		BackpropagatorScorePanel bpsp = BackpropagatorScorePanel.getInstance("Training", "Time", "Deviation", 1000*3600*6, backpropagator);
		p.setPanel(1, 0, bpsp);

		BackpropagatorControlPanel bpcp = new BackpropagatorControlPanel(backpropagator, bpsp, tester);
		p.setPanel(1, 1, bpcp);

		JFrame f1 = new JFrame();
		f1.setSize(Math.min(1200, 4*(extra+(matrix1.length*each))), Math.min(800, extra+(matrix1[0].length*each)));
		f1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f1.setVisible(true);
		f1.setLocation(40, 30);
		f1.getContentPane().add(p);
		f1.invalidate();
		f1.validate();

		while(!stop) {
			try { 
				Thread.sleep(3000); 
			} catch (InterruptedException e) { }
			hm1.updateData();	  
			hm2.updateData();	
			hmO.updateData();	
			bpsp.updateData();
			bpcp.updateData();
		}
		System.out.println("Slut stop="+stop);

	}

	public static final int extra = 60;
	public static final int each = 30;

	private static boolean stop = false;

}
