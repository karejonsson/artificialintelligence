package se.prv.ai.neuralnet.example.digit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import se.prv.ai.neuralnet.training.TrainingData;
import se.prv.ai.neuralnet.training.TrainingDataGenerator;

public class DigitTrainingDataGeneratorStatefulRandom implements TrainingDataGenerator {

    private List<DigitImage> digitImages;
    private int pos = 0;
    private int defaultTrainingSetSize;
	private long datasGotten = 0;

	/*
    public DigitTrainingDataGeneratorStatefulRandom(List<DigitImage> digitImages) {
    	this(digitImages, 10);
    }
	*/
	
    public DigitTrainingDataGeneratorStatefulRandom(List<DigitImage> digitImages, int defaultTrainingSetSize) {
        this.digitImages = digitImages;
        Collections.shuffle(this.digitImages);
        this.defaultTrainingSetSize = defaultTrainingSetSize;
    }
    
    @Override
    public int getDataSize() {
    	return digitImages.size();
    }
    
	@Override
	public int getDefaultSetSize() {
		return defaultTrainingSetSize;
	}

	@Override
	public void setDefaultSetSize(int defaultTrainingSetSize) {
		this.defaultTrainingSetSize = defaultTrainingSetSize;
	}

	@Override
    public TrainingData getData() {
    	return getData(defaultTrainingSetSize);
    }

	@Override
	public long getDatasGotten() {
		return datasGotten;
	}
	
	@Override
    public TrainingData getData(int amount) {
		datasGotten += amount;
        double[][] inputs = new double[amount][DigitImageLoadingService.ROWS * DigitImageLoadingService.COLUMNS];
        double[][] outputs = new double[amount][10];

        for(int i = 0; i < amount; i++) {
        	DigitImage di = getDigitImage();
            inputs[i] = di.getData();
            outputs[i] = getOutputFor(di.getLabel());
        }

        return new TrainingData(inputs, outputs);
    }
	
	private DigitImage getDigitImage() {
		if(pos == digitImages.size()) {
	        Collections.shuffle(digitImages);
	        pos = 0;
		}
		return digitImages.get(pos++);
	}

    private double[] getOutputFor(int label) {
        double[] output = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        output[label] = 1;
        return output;
    }

}
