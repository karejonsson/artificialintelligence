package se.prv.ai.neuralnet.example.simple;

import java.util.Random;

import se.prv.ai.neuralnet.training.TrainingData;
import se.prv.ai.neuralnet.training.TrainingDataGenerator;

/**
 * Created by IntelliJ IDEA.
 * User: vivin
 * Date: 11/11/11
 * Time: 6:18 PM
 */
public class XorTrainingDataGenerator implements TrainingDataGenerator {

	private double[][] inputs = {{0, 0}, {0, 1}, {1, 0}, {1, 1}};
	private double[][] outputs = {{0}, {1}, {1}, {0}};
	private int[] inputIndices = {0, 1, 2, 3};
	private long datasGotten = 0;

    public TrainingData getData() {
    	datasGotten += 4;
        double[][] randomizedInputs = new double[4][2];
        double[][] randomizedOutputs = new double[4][1];

        inputIndices = shuffle(inputIndices);

        for(int i = 0; i < inputIndices.length; i++) {
            randomizedInputs[i] = inputs[inputIndices[i]];
            randomizedOutputs[i] = outputs[inputIndices[i]];
        }

        return new TrainingData(randomizedInputs, randomizedOutputs);
    }

    private int[] shuffle(int[] array) {

        Random random = new Random();
        for(int i = array.length - 1; i > 0; i--) {

            int index = random.nextInt(i + 1);

            int temp = array[i];
            array[i] = array[index];
            array[index] = temp;
        }

        return array;
    }

	@Override
	public int getDataSize() {
		return 4;
	}

	@Override
	public int getDefaultSetSize() {
		return 4;
	}

	@Override
	public void setDefaultSetSize(int sizeOfSet) {
	}

	@Override
	public TrainingData getData(int sizeOfSet) {
		return getData();
	}

	@Override
	public long getDatasGotten() {
		return datasGotten;
	}

}
