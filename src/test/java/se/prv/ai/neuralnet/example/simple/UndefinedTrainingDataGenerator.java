package se.prv.ai.neuralnet.example.simple;

import java.util.Random;

import se.prv.ai.neuralnet.training.TrainingData;
import se.prv.ai.neuralnet.training.TrainingDataGenerator;

public class UndefinedTrainingDataGenerator implements TrainingDataGenerator {

	private double[][] inputs = 
		{ 
			{ 0, 0, 0 }, 
			{ 0, 0, 1 }, 
			{ 0, 1, 0 }, 
			{ 0, 1, 1 }, 
			{ 1, 0, 0 }, 
			{ 1, 0, 1 }, 
			{ 1, 1, 0 },
			{ 1, 1, 1 } 
		};

	private double[][] outputs = 
		{ 
			{ 0 }, 
			{ 1 }, 
			{ 1 }, 
			{ 0 }, 
			{ 0 }, 
			{ 1 }, 
			{ 0 }, 
			{ 1 }
		};

	private int[] inputIndices = { 0, 1, 2, 3, 4, 5, 6, 7 };
	
	private long datasGotten = 0;

	public TrainingData getData() {
		datasGotten += 8;
		double[][] randomizedInputs = new double[8][3];
		double[][] randomizedOutputs = new double[8][1];
		inputIndices = shuffle(inputIndices);
		for (int i = 0; i < inputIndices.length; i++) {
			randomizedInputs[i] = inputs[inputIndices[i]];
			randomizedOutputs[i] = outputs[inputIndices[i]];
		}
		return new TrainingData(randomizedInputs, randomizedOutputs);
	}

	private int[] shuffle(int[] array) {
		Random random = new Random();
		for (int i = array.length - 1; i > 0; i--) {
			int index = random.nextInt(i + 1);
			int temp = array[i];
			array[i] = array[index];
			array[index] = temp;
		}
		return array;
	}

	@Override
	public int getDataSize() {
		return 8;
	}

	@Override
	public int getDefaultSetSize() {
		return 8;
	}

	@Override
	public void setDefaultSetSize(int sizeOfSet) {
	}

	@Override
	public TrainingData getData(int sizeOfSet) {
		return getData();
	}

	@Override
	public long getDatasGotten() {
		return datasGotten;
	}

}
