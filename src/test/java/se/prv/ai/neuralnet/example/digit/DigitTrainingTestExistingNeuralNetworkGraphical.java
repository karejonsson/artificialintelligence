package se.prv.ai.neuralnet.example.digit;

import java.awt.Color;
import java.io.IOException;
import java.util.Arrays;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import general.reuse.swingutils.colors.VariousGenerators;
import general.reuse.swingutils.illustration.HeatMap;
import general.reuse.swingutils.illustration.HeatSpot;
import general.reuse.swingutils.paneling.PanelWithClosePlaces;
import se.prv.ai.neuralnet.core.DefaultSynapseFactory;
import se.prv.ai.neuralnet.core.NeuralNetwork;
import se.prv.ai.neuralnet.training.TrainingData;

public class DigitTrainingTestExistingNeuralNetworkGraphical {

	public static void main(String args[]) {
		NeuralNetwork neuralNetwork = NeuralNetwork.deserialize("DigitRecognizingNeuralNetworkbyKåre-1523270909108.neuralnet", new DefaultSynapseFactory());
		
		DigitTrainingDataGeneratorStatefulRandom testDataGenerator;
		try {
			DigitImageLoadingService testService = new DigitImageLoadingService("/test/t10k-labels-idx1-ubyte.dat", "/test/t10k-images-idx3-ubyte.dat");
			testDataGenerator = new DigitTrainingDataGeneratorStatefulRandom(testService.loadDigitImages(), 600);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		TrainingData testData = testDataGenerator.getData(testDataGenerator.getDataSize());
		PanelWithClosePlaces p = new PanelWithClosePlaces(1, 2);

		p.setPanel(0, 0, getControlPanel(testData, p, neuralNetwork));
		//p.setPanel(0, 1, getHeatMap(testData.getInputs()[0]));
		
		JFrame f1 = new JFrame();
		f1.setSize(1200, 600);
		f1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f1.setVisible(true);
		f1.setLocation(40, 30);
		f1.getContentPane().add(p);
		f1.invalidate();
		f1.validate();
	}
	
	private static JPanel getHeatMap(double[] input) {
		HeatSpot[][] picture = new HeatSpot[DigitImageLoadingService.ROWS][DigitImageLoadingService.COLUMNS];
		for(int i = 0 ; i < input.length ; i++) {
			int ver = i / DigitImageLoadingService.COLUMNS;
			int hor = i % DigitImageLoadingService.COLUMNS;
			picture[hor][ver] = getHeatSpot(input[i]);
		}
		VariousGenerators vg = new VariousGenerators(Color.red, Color.blue);
		HeatMap hm = new HeatMap(
				picture, 
				true, 
				vg.getColors(100));
		return hm;
	}
	
	private static HeatSpot getHeatSpot(final double d) {
		return new HeatSpot() {
			@Override
			public double getHeat() {
				return d;
			}
		};
	}

	private static JPanel getControlPanel(TrainingData testData, PanelWithClosePlaces p, NeuralNetwork neuralNetwork) {
		return new PanelToLookAtPattern(testData, p, neuralNetwork);
	}
	
	public static class PanelToLookAtPattern extends JPanel {
		
		private int pos = 0;
		private TrainingData testData = null;
		private PanelWithClosePlaces p = null;
		private NeuralNetwork neuralNetwork = null;
		private JLabel current = new JLabel();
		private JLabel correct = new JLabel();
		private JLabel evaluated = new JLabel();
		private JLabel statement = new JLabel();
		private JButton forward = new JButton("Forward");
		private JButton backward = new JButton("Backward");
		
		public PanelToLookAtPattern(TrainingData testData, PanelWithClosePlaces p, NeuralNetwork neuralNetwork) {
			this.testData = testData;
			this.p = p;
			this.neuralNetwork = neuralNetwork;
			
			setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			add(current);
			add(correct);
			add(evaluated);
			add(statement);
			add(forward);
			add(backward);
			
			forward.addActionListener(e -> forward());
			backward.addActionListener(e -> backward());
			set();
		}
		
		private void backward() {
			pos = (pos - 1) % testData.getInputs().length;
			set();
		}

		private void forward() {
			pos = (pos + 1) % testData.getInputs().length;
			set();
		}

		private void set() {
			double inputs[] = testData.getInputs()[pos];
			p.setPanel(0, 1, getHeatMap(inputs));
			correct.setText(Arrays.toString(testData.getOutputs()[pos]));
			current.setText(""+pos);
			neuralNetwork.setInputs(inputs);

			double correctoutput[] = testData.getOutputs()[pos];
			int digit = 0;
			for(int j = 0; j < correctoutput.length; j++) {
				if(correctoutput[j] > 0.9) {
					digit = j;
				}
			}

			double evaloutput[] = neuralNetwork.getOutput();
			evaluated.setText(Arrays.toString(evaloutput));
			double max = evaloutput[0];
			double recognizedDigit = 0;

			for(int j = 0; j < evaloutput.length; j++) {
				if(evaloutput[j] > max) {
					max = evaloutput[j];
					recognizedDigit = j;
				}
			}
			statement.setText("Recognized "+digit+" as " + recognizedDigit + ". Corresponding output value was " + max);
		}
		
	}
	
}
