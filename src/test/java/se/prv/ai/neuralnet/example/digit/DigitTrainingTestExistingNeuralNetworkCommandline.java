package se.prv.ai.neuralnet.example.digit;

import java.io.IOException;

import se.prv.ai.neuralnet.core.DefaultSynapseFactory;
import se.prv.ai.neuralnet.core.NeuralNetwork;
import se.prv.ai.neuralnet.training.TrainingData;

public class DigitTrainingTestExistingNeuralNetworkCommandline {

	public static void main(String args[]) {
		NeuralNetwork neuralNetwork = NeuralNetwork.deserialize("DigitRecognizingNeuralNetwork-1522390566425.neuralnet", new DefaultSynapseFactory());
		
		DigitTrainingDataGeneratorStatelessRandom testDataGenerator;
		try {
			DigitImageLoadingService testService = new DigitImageLoadingService("/test/t10k-labels-idx1-ubyte.dat", "/test/t10k-images-idx3-ubyte.dat");
			testDataGenerator = new DigitTrainingDataGeneratorStatelessRandom(testService.loadDigitImages());
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		TrainingData testData = testDataGenerator.getTrainingDataUnshuffled();

		for(int i = 0; i < testData.getInputs().length; i++) {
			double[] input = testData.getInputs()[i];
			double[] output = testData.getOutputs()[i];

			int digit = 0;
			boolean found = false;
			while(digit < 10 && !found) {
				found = (output[digit] == 1);
				digit++;
			}

			neuralNetwork.setInputs(input);
			double[] receivedOutput = neuralNetwork.getOutput();

			double max = receivedOutput[0];
			double recognizedDigit = 0;
			for(int j = 0; j < receivedOutput.length; j++) {
				if(receivedOutput[j] > max) {
					max = receivedOutput[j];
					recognizedDigit = j;
				}
			}

			System.out.println("Recognized " + (digit - 1) + " as " + recognizedDigit + ". Corresponding output value was " + max);
		}

	}

}
