package se.prv.ai.neuralnet.example.digit;

import java.util.*;

import se.prv.ai.neuralnet.training.TrainingData;
import se.prv.ai.neuralnet.training.TrainingDataGenerator;

/**
 * Created by IntelliJ IDEA.
 * User: vivin
 * Date: 11/11/11
 * Time: 6:09 PM
 */
public class DigitTrainingDataGeneratorStatelessRandom implements TrainingDataGenerator {

    private Map<Integer, List<DigitImage>> labelToDigitImageListMap;
    private int[] digits = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    private int defaultTrainingSetSize;
    private int trainingDataSize = 0;
	private long datasGotten = 0;

    public DigitTrainingDataGeneratorStatelessRandom(List<DigitImage> digitImages) {
    	this(digitImages, 10);
    }

    public DigitTrainingDataGeneratorStatelessRandom(List<DigitImage> digitImages, int defaultTrainingSetSize) {
        labelToDigitImageListMap = new HashMap<Integer, List<DigitImage>>();
        this.defaultTrainingSetSize = defaultTrainingSetSize;

        for (DigitImage digitImage: digitImages) {
        	int label = digitImage.getLabel();
        	List<DigitImage> samples = labelToDigitImageListMap.get(label);
            if (samples == null) {
            	samples = new ArrayList<DigitImage>();
                labelToDigitImageListMap.put(label, samples);
            }
            samples.add(digitImage);
        }
        
        for(List<DigitImage> set : labelToDigitImageListMap.values()) {
        	trainingDataSize += set.size();
        }
    }
    
    @Override
    public int getDataSize() {
    	return trainingDataSize;
    }
    
	@Override
	public int getDefaultSetSize() {
		return defaultTrainingSetSize;
	}

	@Override
	public void setDefaultSetSize(int defaultTrainingSetSize) {
		this.defaultTrainingSetSize = defaultTrainingSetSize;
	}

	@Override
    public TrainingData getData() {
    	return getData(defaultTrainingSetSize);
    }
	
	@Override
	public long getDatasGotten() {
		return datasGotten;
	}
	
	@Override
    public TrainingData getData(int amount) {
		datasGotten += amount;
        digits = shuffle(digits);

        double[][] inputs = new double[amount][DigitImageLoadingService.ROWS * DigitImageLoadingService.COLUMNS];
        double[][] outputs = new double[amount][10];

        for(int i = 0; i < amount; i++) {
            inputs[i] = getRandomImageForLabel(digits[i % 10]).getData();
            outputs[i] = getOutputFor(digits[i % 10]);
        }

        return new TrainingData(inputs, outputs);
    }

    public TrainingData getTrainingDataUnshuffled() {
        double[][] inputs = new double[10][DigitImageLoadingService.ROWS * DigitImageLoadingService.COLUMNS];
        double[][] outputs = new double[10][10];

        for(int i = 0; i < 10; i++) {
            inputs[i] = getImageForLabel(digits[i], i).getData();
            outputs[i] = getOutputFor(digits[i]);
        }

        return new TrainingData(inputs, outputs);
    }

    private int[] shuffle(int[] array) {

        Random random = new Random();
        for(int i = array.length - 1; i > 0; i--) {

            int index = random.nextInt(i + 1);

            int temp = array[i];
            array[i] = array[index];
            array[index] = temp;
        }

        return array;
    }

    private DigitImage getRandomImageForLabel(int label) {
        Random random = new Random();
        List<DigitImage> images = labelToDigitImageListMap.get(label);
        return images.get(random.nextInt(images.size()));
    }

    private DigitImage getImageForLabel(int label, int idx) {
        List<DigitImage> images = labelToDigitImageListMap.get(label);
        return images.get(Math.min(idx, images.size()-1));
    }

    private double[] getOutputFor(int label) {
        double[] output = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        output[label] = 1;
        return output;
    }

}
