package se.prv.ai.neuralnet.example.simple;

import java.awt.Color;

import javax.swing.JFrame;

import general.reuse.swingutils.colors.VariousGenerators;
import general.reuse.swingutils.illustration.HeatMap;
import general.reuse.swingutils.illustration.HeatSpot;
import general.reuse.swingutils.paneling.PanelWithClosePlaces;
import se.prv.ai.neuralnet.activators.SigmoidActivationStrategy;
import se.prv.ai.neuralnet.core.Layer;
import se.prv.ai.neuralnet.core.NeuralNetwork;
import se.prv.ai.neuralnet.core.Neuron;
import se.prv.ai.neuralnet.gui.BackpropagatorControlPanel;
import se.prv.ai.neuralnet.gui.BackpropagatorScorePanel;
import se.prv.ai.neuralnet.gui.HeatGivingSynapseFactory;
import se.prv.ai.neuralnet.training.Backpropagator;
import se.prv.ai.neuralnet.training.TrainingDataGenerator;

public class NeuralNetKare {
	
	private static HeatGivingSynapseFactory sf1;
	private static HeatGivingSynapseFactory sf2;
	
    public static NeuralNetwork createUntrainedNeuralNetwork(int ins, int middle) {
        NeuralNetwork testNeuralNetwork = new NeuralNetwork("Trained Weird Network");

        Neuron inputBias = new Neuron(new SigmoidActivationStrategy());
        inputBias.setOutput(1);
        Layer inputLayer = new Layer(inputBias);

        for(int i = 0 ; i < ins ; i++) {
            Neuron n = new Neuron(new SigmoidActivationStrategy());
            n.setOutput(0);
            inputLayer.addNeuron(n);
        }

        Neuron bias = new Neuron(new SigmoidActivationStrategy());
        bias.setOutput(1);
        sf1 = new HeatGivingSynapseFactory("I1");
        Layer hiddenLayer = new Layer(sf1, inputLayer, bias);

        for(int i = 0 ; i < middle ; i++) {
            Neuron n = new Neuron(new SigmoidActivationStrategy());
            hiddenLayer.addNeuron(n);
        }

        sf2 = new HeatGivingSynapseFactory("I2");
        Layer outputLayer = new Layer(sf2, hiddenLayer);
        Neuron outNeuron = new Neuron(new SigmoidActivationStrategy());
        outputLayer.addNeuron(outNeuron);

        testNeuralNetwork.addLayer(inputLayer);
        testNeuralNetwork.addLayer(hiddenLayer);
        testNeuralNetwork.addLayer(outputLayer);

        return testNeuralNetwork;
    }

    private static boolean stop = false;
    
    public static void main(String[] args) {
    	
        final NeuralNetwork neuralnet = createUntrainedNeuralNetwork(3, 5);
        final TrainingDataGenerator dataGenerator = new UndefinedTrainingDataGenerator();
        
        final Backpropagator backpropagator = new Backpropagator(neuralnet, 0.1, 0.9, 0);

        Runnable trainer = new Runnable() {
			public void run() {
		        backpropagator.train(dataGenerator, 0.0001);
		        
		        stop = true;

		        System.out.println("SF1 "+sf1);
		        System.out.println("SF2 "+sf1);

		        neuralnet.setInputs(new double[]{0, 0, 0});
		        System.out.println("0 0 0: " + (NeuralNetKare.toString(neuralnet.getOutput())));

		        neuralnet.setInputs(new double[]{0, 0, 1});
		        System.out.println("0 0 1: " + (NeuralNetKare.toString(neuralnet.getOutput())));

		        neuralnet.setInputs(new double[]{0, 1, 0});
		        System.out.println("0 1 0: " + (NeuralNetKare.toString(neuralnet.getOutput())));

		        neuralnet.setInputs(new double[]{0, 1, 1});
		        System.out.println("0 1 1: " + (NeuralNetKare.toString(neuralnet.getOutput())));
		        
		        neuralnet.setInputs(new double[]{1, 0, 0});
		        System.out.println("1 0 0: " + (NeuralNetKare.toString(neuralnet.getOutput())));

		        neuralnet.setInputs(new double[]{1, 0, 1});
		        System.out.println("1 0 1: " + (NeuralNetKare.toString(neuralnet.getOutput())));

		        neuralnet.setInputs(new double[]{1, 1, 0});
		        System.out.println("1 1 0: " + (NeuralNetKare.toString(neuralnet.getOutput())));

		        neuralnet.setInputs(new double[]{1, 1, 1});
		        System.out.println("1 1 1: " + (NeuralNetKare.toString(neuralnet.getOutput())));		        
			}        	
        };
        new Thread(trainer).start();
        makeIllustration(sf1, sf2, backpropagator);
    }
    
	public static String toString(double d[]) {
    	if(d == null) {
    		return "[]";
    	}
    	if(d.length == 1) {
    		return "["+d[0]+"]";
    	}
    	StringBuffer sb = new StringBuffer("["+d[0]);
    	for(int i = 1 ; i < d.length ; i++) {
    		sb.append(", "+d[i]);
    	}
    	return sb.append("]").toString();
    }

	public static final int extra = 60;
	public static final int each = 30;
	
	private static void makeIllustration(HeatGivingSynapseFactory sf1, HeatGivingSynapseFactory sf2, Backpropagator backpropagator) {
		PanelWithClosePlaces p = new PanelWithClosePlaces(1, 4);

		HeatSpot[][] matrix1 = sf1.getHeatGrid();
		VariousGenerators vg1 = new VariousGenerators(Color.red, Color.blue);
		HeatMap hm1 = new HeatMap(
				matrix1, 
				true, 
				vg1.getColors(100)); 
		p.setPanel(0, 0, hm1);
		
		HeatSpot[][] matrix2 = sf2.getHeatGrid();
		VariousGenerators vg2 = new VariousGenerators(Color.red, Color.blue);
		HeatMap hm2 = new HeatMap(
				matrix2, 
				true, 
				vg2.getColors(100)); 
		p.setPanel(0, 1, hm2);
		
		BackpropagatorScorePanel bpsp = BackpropagatorScorePanel.getInstance("Training", "Time", "Deviation", 1000*30, backpropagator);
		p.setPanel(0, 2, bpsp);
		
		BackpropagatorControlPanel bpcp = new BackpropagatorControlPanel(backpropagator, null, null);
		p.setPanel(0, 3, bpcp);
		
		JFrame f1 = new JFrame();
		f1.setSize(Math.min(1200, 3*(extra+(matrix1.length*each))), Math.min(800, extra+(matrix1[0].length*each)));
		f1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f1.setVisible(true);
		f1.setLocation(40, 30);
		f1.getContentPane().add(p);
		f1.invalidate();
		f1.validate();
		
		int ctr = 0;
		while(!stop) {
			try { 
				if(ctr++ > 60) {
					printMatrix("1", matrix1);
					printMatrix("2", matrix2);
					ctr = 0;
				}
				Thread.sleep(50); 
			} catch (InterruptedException e) { }
			hm1.updateData();	  
			hm2.updateData();	
			bpsp.updateData();
		}
		System.out.println("Slut");
	}

	private static void printMatrix(String name, HeatSpot[][] matrix) {
		StringBuffer g = new StringBuffer();
		g.append("{\n");
		for(int i = 0 ; i < matrix.length ; i++) {
			g.append("  {");
			for(int ii = 0 ; ii < matrix[i].length ; ii++) {
				g.append(" "+((matrix[i][ii] != null) ? String.format("%1$,.2f", matrix[i][ii].getHeat()) : "_.__"));
			}
			g.append(" }\n");
		}
		g.append("}");
		System.out.println(name+g.toString());
	}
    
}
