package se.prv.ai.neuralnet.core;

import java.io.Serializable;

public interface SynapseFactory extends Serializable {
	
	public Synapse produceFactory(Neuron sourceNeuron, double weight, Integer xpos, Integer ypos);

}
