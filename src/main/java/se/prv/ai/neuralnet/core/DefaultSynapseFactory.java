package se.prv.ai.neuralnet.core;

import java.io.Serializable;

public class DefaultSynapseFactory implements SynapseFactory, Serializable {

	public Synapse produceFactory(Neuron sourceNeuron, double weight, Integer xpos, Integer ypos) {
		return new Synapse(sourceNeuron, weight);
	}

}
