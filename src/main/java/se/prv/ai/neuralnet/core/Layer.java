package se.prv.ai.neuralnet.core;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: vivin
 * Date: 11/5/11
 * Time: 12:42 PM
 */
public class Layer implements Serializable {

    private List<Neuron> neurons;
    private Layer previousLayer;
    private Layer nextLayer;
    private Neuron bias;
    private SynapseFactory sf;

    public Layer() {
    	this(new DefaultSynapseFactory(), null, null);
    }

    public Layer(SynapseFactory sf) {
    	this(sf, null, null);
    }

    public Layer(Layer previousLayer) {
        this(new DefaultSynapseFactory(), previousLayer, null);
    }

    public Layer(SynapseFactory sf, Layer previousLayer) {
        this(sf, previousLayer, null);
    }

    public Layer(Neuron bias) {
        this(new DefaultSynapseFactory(), null, bias);
    }

    public Layer(Layer previousLayer, Neuron bias) {
        this(new DefaultSynapseFactory(), previousLayer, bias);
    }

    public Layer(SynapseFactory sf, Layer previousLayer, Neuron bias) {
    	this.sf = sf;
        this.previousLayer = previousLayer;
        this.bias = bias;
        neurons = new ArrayList<Neuron>();
        if(bias != null) {
            neurons.add(bias);
        }
    }

    public SynapseFactory getSf() {
		return sf;
	}

	public void setSf(SynapseFactory sf) {
		this.sf = sf;
	}

	public List<Neuron> getNeurons() {
        return this.neurons;
    }

    public void addNeuron(Neuron neuron) {

        neurons.add(neuron);
        Integer ypos = neurons.size()-1;
        if(bias != null) {
        	ypos--;
        }

        if(previousLayer != null) {
        	int xpos = 0;
            for(Neuron previousLayerNeuron : previousLayer.getNeurons()) {
                neuron.addInput(sf.produceFactory(previousLayerNeuron, (Math.random() * 1) - 0.5, xpos, ypos)); //initialize with a random weight between -1 and 1
                //neuron.addInput(new Synapse(previousLayerNeuron, (Math.random() * 1) - 0.5)); //initialize with a random weight between -1 and 1
                xpos++;
            }
        }
    }

    public void addNeuron(Neuron neuron, double[] weights) {

        neurons.add(neuron);
        Integer ypos = neurons.size()-1;
        if(bias != null) {
        	ypos--;
        }

        if(previousLayer != null) {

            if(previousLayer.getNeurons().size() != weights.length) {
                throw new IllegalArgumentException("The number of weights supplied must be equal to the number of neurons in the previous layer");
            }

            else {
            	int xpos = 0;
                List<Neuron> previousLayerNeurons = previousLayer.getNeurons();
                for(int i = 0; i < previousLayerNeurons.size(); i++) {
                    neuron.addInput(sf.produceFactory(previousLayerNeurons.get(i), weights[i], xpos, ypos));
                    //neuron.addInput(new Synapse(previousLayerNeurons.get(i), weights[i]));
                    xpos++;
                }
            }

        }
    }

    public void feedForward() {

        int biasCount = hasBias() ? 1 : 0;

        for(int i = biasCount; i < neurons.size(); i++) {
            neurons.get(i).activate();
        }
    }

    public Layer getPreviousLayer() {
        return previousLayer;
    }

    void setPreviousLayer(Layer previousLayer) {
        this.previousLayer = previousLayer;
    }

    public Layer getNextLayer() {
        return nextLayer;
    }

    void setNextLayer(Layer nextLayer) {
        this.nextLayer = nextLayer;
    }

    public boolean isOutputLayer() {
        return nextLayer == null;
    }

    public boolean hasBias() {
        return bias != null;
    }
    
}
