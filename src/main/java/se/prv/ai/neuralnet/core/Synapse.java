package se.prv.ai.neuralnet.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: vivin
 * Date: 11/5/11
 * Time: 12:03 PM
 */
public class Synapse implements Serializable {
	
	private static final long serialVersionUID = 1232153449802753058L;
	
	private static int historylength = 10;
	
	public static void setHistoryLength(int hl) {
		historylength = hl;
	}

    private Neuron sourceNeuron;
    private double weight;
    private double[] history;
    private int historypos = 0;

    public Synapse(Neuron sourceNeuron, double weight) {
        this.sourceNeuron = sourceNeuron;
        history = new double[historylength];
        setWeight(weight);
    }

    public Neuron getSourceNeuron() {
        return sourceNeuron;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
        history[historypos] = weight;
        historypos = (historypos+1) % historylength;
    }
    
    public double[] getHistory() {
    	return history;
    }
    
}
