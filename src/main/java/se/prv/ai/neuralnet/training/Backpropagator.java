package se.prv.ai.neuralnet.training;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.prv.ai.neuralnet.core.Layer;
import se.prv.ai.neuralnet.core.NeuralNetwork;
import se.prv.ai.neuralnet.core.Neuron;
import se.prv.ai.neuralnet.core.Synapse;

/**
 * Created by IntelliJ IDEA.
 * User: vivin
 * Date: 11/6/11
 * Time: 12:19 PM
 */
public class Backpropagator {

	private NeuralNetwork neuralNetwork;
	private double learningRate;
	private double momentum;
	private double characteristicTime;
	private double currentEpoch;
	private double error;
	private double average = 25;
	private int epoch = 1;
	private double newLearningRate;
	private TrainingDataGenerator generator;
	private double errorThreshold;
	private double sum = 0.0;
	private int samples = 25;
	private double[] errors = new double[samples];
	private boolean interrupt = false;
	private boolean paused = false;
	
	public Backpropagator(NeuralNetwork neuralNetwork, double learningRate, double momentum, double characteristicTime) {
		this.neuralNetwork = neuralNetwork;
		this.learningRate = learningRate;
		this.momentum = momentum;
		this.characteristicTime = characteristicTime;
		updateNewLearningrate();
	}
	
	public long getTrainingsDone() {
		return generator.getDatasGotten();
	}

	public NeuralNetwork getNeuralNetwork() {
		return neuralNetwork;
	}

	public double getError() {
		return error;
	}

	public double getAverage() {
		return average;
	}

	public double getEpoch() {
		return epoch;
	}

	public double getLearningRate() {
		return learningRate;
	}

	public void setLearningRate(double learningRate) {
		this.learningRate = learningRate;
		updateNewLearningrate();
	}

	public double getMomentum() {
		return momentum;
	}

	public void setMomentum(double momentum) {
		this.momentum = momentum;
	}

	public double getCharacteristicTime() {
		return characteristicTime;
	}

	public void setCharacteristicTime(double characteristicTime) {
		this.characteristicTime = characteristicTime;
		updateNewLearningrate();
	}
	
	public double getErrorThreshold() {
		return errorThreshold;
	}

	public void setErrorThreshold(double errorThreshold) {
		this.errorThreshold = errorThreshold;
	}

	public void setGenerator(TrainingDataGenerator generator) {
		this.generator = generator;
	}

	private void updateNewLearningrate() {
		newLearningRate = characteristicTime > 0 ? learningRate / (1 + (currentEpoch / characteristicTime)) : learningRate;		
	}
	
	public void train(TrainingDataGenerator generator, double errorThreshold) {
		setErrorThreshold(errorThreshold);
		setGenerator(generator);
		train();
	}
	
	public int getTrainingDataSize() {
		return generator.getDataSize();
	}
	
	public int getDefaultTrainingSetSize() {
		return generator.getDefaultSetSize();
	}
	
	public void setDefaultTrainingSetSize(int defaultTrainingSetSize) {
		generator.setDefaultSetSize(defaultTrainingSetSize);
	}
	
	public boolean isInterrupted() {
		return interrupt;
	}

	public void setInterrupted(boolean interrupt) {
		this.interrupt = interrupt;
	}

	public boolean isPaused() {
		return paused;
	}

	public void setPaused(boolean paused) {
		this.paused = paused;
	}

	public void train() {
		do {
			TrainingData trainingData = generator.getData();
			error = backpropagate(trainingData.getInputs(), trainingData.getOutputs());

			int idx = epoch % samples;
			sum -= errors[idx];
			errors[idx] = error;
			sum += errors[idx];

			if(epoch > samples) {
				average = sum / samples;
			}
			epoch++;
			currentEpoch = epoch;
			updateNewLearningrate();
			
			while(paused && !interrupt) {
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} while(average > errorThreshold && !interrupt);
		//System.out.println("average "+average);
		//System.out.println("errorThreshold "+errorThreshold);
		//System.out.println("interrupt "+interrupt);
	}
	
	private void backpropagateOutputLayer(List<Neuron> neurons, double[] output, double[] expectedOutput) {
		for (int k = 0; k < neurons.size(); k++) {
			Neuron neuron = neurons.get(k);
			double neuronError = neuron.getDerivative() * (output[k] - expectedOutput[k]);
			neuron.setError(neuronError);
		}							
	}

	private void backpropagateHiddenLayer(Layer layer, List<Neuron> neurons) {
		Layer nextLayer = layer.getNextLayer();
		for (int k = 0; k < neurons.size(); k++) {
			Neuron neuron = neurons.get(k);
			double neuronError = neuron.getDerivative();
			double sum = 0;
			List<Neuron> downstreamNeurons = nextLayer.getNeurons();
			for (Neuron downstreamNeuron : downstreamNeurons) {

				int l = 0;
				boolean found = false;
				List<Synapse> inputSynapses = downstreamNeuron.getInputs();
				while (l < inputSynapses.size() && !found) {
					Synapse synapse = inputSynapses.get(l);

					if (synapse.getSourceNeuron() == neuron) {
						sum += (synapse.getWeight() * downstreamNeuron.getError());
						found = true;
					}

					l++;
				}
			}
			neuronError *= sum;
			neuron.setError(neuronError);
		}					
	}

	public double backpropagate(double[][] inputs, double[][] expectedOutputs) {

		double error = 0;

		for (int i = 0; i < inputs.length; i++) {

			double[] input = inputs[i];
			double[] expectedOutput = expectedOutputs[i];

			neuralNetwork.setInputs(input);
			double[] output = neuralNetwork.getOutput();

			List<Layer> layers = neuralNetwork.getLayers();

			// First step of the backpropagation algorithm. Backpropagate errors from the 
			// output layer all the way up to the first hidden layer
			for (int j = layers.size() - 1; j > 0; j--) {
				Layer layer = layers.get(j);
				List<Neuron> neurons = layer.getNeurons();
				if (layer.isOutputLayer()) {
					backpropagateOutputLayer(neurons, output, expectedOutput);					
				}
				else {
					backpropagateHiddenLayer(layer, neurons);
				}
			}

			// Second step of the backpropagation algorithm. Using the errors calculated above, 
			// update the weights of the network
			Map<Synapse, Double> synapseNeuronDeltaMap = new HashMap<Synapse, Double>();

			for(int j = layers.size() - 1; j > 0; j--) {
				Layer layer = layers.get(j);

				for(Neuron neuron : layer.getNeurons()) {
					updateSynapses(synapseNeuronDeltaMap, neuron);
				}
			}

			output = neuralNetwork.getOutput();
			error += error(output, expectedOutput);
		}

		return error;
	}
	
	private void updateSynapses(Map<Synapse, Double> synapseNeuronDeltaMap, Neuron neuron) {
		for(Synapse synapse : neuron.getInputs()) {

			double delta = newLearningRate * neuron.getError() * synapse.getSourceNeuron().getOutput();

			Double previousDelta = synapseNeuronDeltaMap.get(synapse);
			if(previousDelta != null) {
				delta += momentum * previousDelta;
			}

			synapseNeuronDeltaMap.put(synapse, delta);
			synapse.setWeight(synapse.getWeight() - delta);
		}
	}

	/*
    private String explode(double[] array) {
        String string = "[";

        for (double number : array) {
            string += number + ", ";
        }

        Pattern pattern = Pattern.compile(", $", Pattern.DOTALL);
        Matcher matcher = pattern.matcher(string);
        string = matcher.replaceAll("");

        return string + "]";
    }
	 */

	public double error(double[] actual, double[] expected) {

		if (actual.length != expected.length) {
			throw new IllegalArgumentException("The lengths of the actual and expected value arrays must be equal");
		}

		double sum = 0;

		for (int i = 0; i < expected.length; i++) {
			sum += Math.pow(expected[i] - actual[i], 2);
		}

		return sum / 2;
	}

}