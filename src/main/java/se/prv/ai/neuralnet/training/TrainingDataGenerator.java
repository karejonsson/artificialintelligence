package se.prv.ai.neuralnet.training;

/**
 * Created by IntelliJ IDEA.
 * User: vivin
 * Date: 11/11/11
 * Time: 6:05 PM
 */
public interface TrainingDataGenerator {
	int getDataSize();
	int getDefaultSetSize();
	void setDefaultSetSize(int sizeOfSet);
    TrainingData getData();
    TrainingData getData(int sizeOfSet);
	long getDatasGotten();
}
