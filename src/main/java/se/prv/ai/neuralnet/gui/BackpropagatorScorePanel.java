package se.prv.ai.neuralnet.gui;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

import se.prv.ai.neuralnet.training.Backpropagator;

public class BackpropagatorScorePanel extends ChartPanel {
	
	private static final long serialVersionUID = 1L;

	public static BackpropagatorScorePanel getInstance(String title, String below, String legend, long millisInGraph, Backpropagator backpropagator) {
		TimeSeriesCollection collection = new TimeSeriesCollection();	
		TimeSeries errorSer = new TimeSeries("Deviation", Second.class);
		TimeSeries averageSer = new TimeSeries("Average", Second.class);
		collection.addSeries(errorSer);
		collection.addSeries(averageSer);

		JFreeChart chart = ChartFactory.createTimeSeriesChart(
				title, 
				below, // Text below chart
				legend, 
				collection,
                true,
                true,
                false
        );
        final XYPlot plot = chart.getXYPlot();
        DateAxis domainaxis = (DateAxis) plot.getDomainAxis();
        domainaxis.setAutoRange(true);
        domainaxis.setFixedAutoRange(millisInGraph);
        domainaxis.setDateFormatOverride(new SimpleDateFormat("HH:mm:ss"));
        NumberAxis valueaxis = (NumberAxis) plot.getRangeAxis();
        valueaxis.setAutoRangeIncludesZero(false);
		return new BackpropagatorScorePanel(chart, errorSer, averageSer, millisInGraph, backpropagator);
	}
	
	private TimeSeries errorSer;
	private TimeSeries averageSer;
	private long millisInGraph;
	private Backpropagator backpropagator;
	private JFreeChart chart;
	
	private BackpropagatorScorePanel(JFreeChart chart, TimeSeries errorSer, TimeSeries averageSer, long millisInGraph, Backpropagator backpropagator) {
		super(chart);
		this.chart = chart;
		this.errorSer = errorSer;
		this.millisInGraph = millisInGraph;
		this.backpropagator = backpropagator;
	}
	
	public long getMillisInGraph() {
		return millisInGraph;
	}

	public void setMillisInGraph(long millisInGraph) {
		this.millisInGraph = millisInGraph;
        final XYPlot plot = chart.getXYPlot();
        DateAxis domainaxis = (DateAxis) plot.getDomainAxis();
        domainaxis.setFixedAutoRange(millisInGraph);
	}
	
	public void addValue(long timeMillis, double error, double average) {
		try {
			errorSer.addOrUpdate(new Second(new Date(timeMillis)), error);
			errorSer.removeAgedItems(timeMillis-millisInGraph, false);
			averageSer.addOrUpdate(new Second(new Date(timeMillis)), average);
			averageSer.removeAgedItems(timeMillis-millisInGraph, false);
		}
		catch(Exception e) {
		}
	}

	public void updateData() {
		addValue(System.currentTimeMillis(), backpropagator.getError(), backpropagator.getAverage());
	}

}
