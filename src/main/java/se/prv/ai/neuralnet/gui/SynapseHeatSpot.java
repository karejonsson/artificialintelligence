package se.prv.ai.neuralnet.gui;

import general.reuse.swingutils.illustration.HeatSpot;
import se.prv.ai.neuralnet.core.Synapse;

public class SynapseHeatSpot implements HeatSpot {
	
	private SynapseHeatSpotCommon common;
	private Synapse syn;
	private double prevweight;
	
	public SynapseHeatSpot(SynapseHeatSpotCommon common, Synapse syn) {
		this.common = common;
		this.syn = syn;
		prevweight = syn.getWeight();
	}
	
	//private int ctr = 0;

	@Override
	public double getHeat() {
		//ctr++;
		double weight = syn.getWeight();
		double out = weight - prevweight;
		prevweight = weight;
		//System.out.println("CTR "+ctr);
		return common.map(out);
	}

}
