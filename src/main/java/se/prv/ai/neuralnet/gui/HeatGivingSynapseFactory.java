package se.prv.ai.neuralnet.gui;

import general.reuse.swingutils.illustration.HeatSpot;
import se.prv.ai.neuralnet.core.Neuron;
import se.prv.ai.neuralnet.core.Synapse;
import se.prv.ai.neuralnet.core.SynapseFactory;

public class HeatGivingSynapseFactory implements SynapseFactory {
	
	private int maxx = 0;
	private int maxy = 0;
	private Synapse[][] grid = new Synapse[maxx][maxy];
	private String name;
	private SynapseHeatSpotCommon common = new SynapseHeatSpotCommon();
	
	public HeatGivingSynapseFactory(String name) {
		this.name = name;
	}
	
	public String toString() {
		StringBuffer g = new StringBuffer();
		g.append("{\n");
		for(int i = 0 ; i < grid.length ; i++) {
			g.append("  {");
			for(int ii = 0 ; ii < grid[i].length ; ii++) {
				g.append(" "+((grid[i][ii] != null) ? "*" : "_"));
			}
			g.append(" }\n");
		}
		g.append("}");
		return name+" X "+maxx+", Y "+maxy+"\n"+g.toString();
	}
	
	public String getName() {
		return name;
	}

	public Synapse produceFactory(Neuron sourceNeuron, double weight, Integer xpos, Integer ypos) {
		if(xpos >= maxx && ypos < maxy) {
			//System.out.println(name+" X-dim maxx "+maxx+", maxy "+maxy+", xpos "+xpos+", ypos "+ypos);
			// Has grown in x-dim
			Synapse[][] newgrid = new Synapse[maxy][];
			for(int y = 0 ; y < maxy ; y++) {
				newgrid[y] = new Synapse[xpos+1];
				System.arraycopy(grid[y], 0, newgrid[y], 0, maxx);
			}
			maxx = xpos+1;
			grid = newgrid;
			grid[ypos][xpos] = new Synapse(sourceNeuron, weight);
			return grid[ypos][xpos];
		}
		if(ypos >= maxy && xpos < maxx) {
			//System.out.println(name+" Y-dim maxx "+maxx+", maxy "+maxy+", xpos "+xpos+", ypos "+ypos);
			// Has grown in y-dim
			Synapse[][] newgrid = new Synapse[ypos+1][];
			System.arraycopy(grid, 0, newgrid, 0, grid.length);
			for(int y = grid.length ; y <= ypos ; y++) {
				newgrid[y] = new Synapse[maxx];
				//System.arraycopy(grid[y], 0, newgrid[y], 0, maxx);
			}
			maxy = ypos+1;
			grid = newgrid;
			grid[ypos][xpos] = new Synapse(sourceNeuron, weight);
			return grid[ypos][xpos];
		}
		if(ypos >= maxy && xpos >= maxx) {
			//System.out.println(name+" X&Y-dim maxx "+maxx+", maxy "+maxy+", xpos "+xpos+", ypos "+ypos);
			// Has grown in x-dima  and y-dim
			Synapse[][] newgrid = new Synapse[ypos+1][];
			for(int y = 0 ; y < maxy ; y++) {
				newgrid[y] = new Synapse[xpos+1];
				System.arraycopy(grid[y], 0, newgrid[y], 0, maxx);
			}
			for(int y = maxy ; y <= ypos ; y++) {
				newgrid[y] = new Synapse[xpos+1];
			}
			maxx = xpos+1;
			maxy = ypos+1;
			grid = newgrid;
			grid[ypos][xpos] = new Synapse(sourceNeuron, weight);
			return grid[ypos][xpos];
		}
		//System.out.println(name+" NO-dim maxx "+maxx+", maxy "+maxy+", xpos "+xpos+", ypos "+ypos);
		grid[ypos][xpos] = new Synapse(sourceNeuron, weight);
		return grid[ypos][xpos];		
	}
	
	public SynapseHeatSpotCommon getCommon() {
		return common;
	}
	
	public HeatSpot[][] getHeatGrid() {
		HeatSpot[][] out = new HeatSpot[maxy][maxx];
		for(int y = 0 ; y < grid.length ; y++) {
			for(int x = 0 ;x < grid[y].length ; x++) {
				out[y][x] = new SynapseHeatSpot(common, grid[y][x]);
			}
		}
		return out;
	}
	
	public int getMaxX() {
		return maxx;
	}

	public int getMaxY() {
		return maxy;
	}

	public static void main(String args[]) {
		HeatGivingSynapseFactory f = new HeatGivingSynapseFactory("A");
		f.produceFactory(null, 0, 0, 1);
		System.out.println(""+f);
		f.produceFactory(null, 0, 1, 1);
		System.out.println(""+f);
		f.produceFactory(null, 0, 2, 1);
		System.out.println(""+f);
		f.produceFactory(null, 0, 0, 2);
		System.out.println(""+f);
		f.produceFactory(null, 0, 1, 2);
		System.out.println(""+f);
		f.produceFactory(null, 0, 2, 2);
		System.out.println(""+f);
	}

	public Synapse getSynapse(int currentHeatSpotX, int currentHeatSpotY) {
		return grid[currentHeatSpotY][currentHeatSpotX];
	}
	
}
