package se.prv.ai.neuralnet.gui;

public class SynapseHeatSpotCommon {
	
	public double map(double out) {
		return 1.0/(1.0 + Math.exp(-out));
	}

}
