package se.prv.ai.neuralnet.gui;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;

import se.prv.ai.neuralnet.training.Backpropagator;

public class BackpropagatorControlPanel extends JPanel {
	
	private Backpropagator backpropagator;
	
	private JSlider learningrateSlider = new JSlider(JSlider.HORIZONTAL, 100, 0); // 0.001 -> 100 dvs -3 -> 2 
	private JSlider momentumSlider = new JSlider(JSlider.HORIZONTAL, 100, 0); // 0.001 -> 100 dvs -3 -> 2 
	private JSlider characteristicTimeSlider = new JSlider(JSlider.HORIZONTAL, 100, 0); // 0.001 -> 100 dvs -3 -> 2 

	private JSlider errorThresholdSlider = new JSlider(JSlider.HORIZONTAL, 100, 0); // 0.001 -> 100 dvs -3 -> 2 
	private JSlider trainingSetSizeSlider = new JSlider(JSlider.HORIZONTAL, 100, 0); 
	private JSlider scoreTimeMillis;
	private JButton pauseButton = new JButton("Pause");
	private JButton interruptButton = new JButton("Quit");
	private JButton saveButton = new JButton("Save NN");
	private JButton testButton = new JButton("Test NN");
	private Runnable test = null;
	private BackpropagatorScorePanel scorePanel;
	private JLabel trainings = new JLabel();
	
	public BackpropagatorControlPanel(Backpropagator backpropagator, BackpropagatorScorePanel scorePanel, Runnable test) {
		this.backpropagator = backpropagator;
		this.test = test;
		this.scorePanel = scorePanel;
		trainingDataSizeLog = Math.log(backpropagator.getTrainingDataSize());
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		learningrateSlider.setValue(fromBackpropperToSliderLearningrate(backpropagator.getLearningRate()));
		learningrateSlider.setToolTipText("Learningrate "+backpropagator.getLearningRate());
		add(learningrateSlider);
		learningrateSlider.addChangeListener(e -> changeLearningrate(e));
		
		momentumSlider.setValue(fromBackpropperToSliderMomentum(backpropagator.getLearningRate()));
		momentumSlider.setToolTipText("Momentum "+backpropagator.getMomentum());
		add(momentumSlider);
		momentumSlider.addChangeListener(e -> changeMomentum(e));
		
		characteristicTimeSlider.setValue(fromBackpropperToSliderCharacteristicTime(backpropagator.getCharacteristicTime()));
		characteristicTimeSlider.setToolTipText("Characteristic time "+backpropagator.getCharacteristicTime());
		add(characteristicTimeSlider);
		characteristicTimeSlider.addChangeListener(e -> changeCharacteristicTime(e));
		
		errorThresholdSlider.setValue(fromBackpropperToSliderErrorThreshold(backpropagator.getErrorThreshold()));
		errorThresholdSlider.setToolTipText("Error Threshold "+backpropagator.getErrorThreshold());
		add(errorThresholdSlider);
		errorThresholdSlider.addChangeListener(e -> changeErrorThreshold(e));
		
		trainingSetSizeSlider.setValue(fromBackpropperToSliderTrainingSetSize(backpropagator.getDefaultTrainingSetSize()));
		trainingSetSizeSlider.setToolTipText("Training set size "+backpropagator.getDefaultTrainingSetSize());
		add(trainingSetSizeSlider);
		trainingSetSizeSlider.addChangeListener(e -> changeTrainingSetSizeSlider(e));
		
		if(scorePanel != null) {
			scoreTimeMillis = new JSlider(JSlider.HORIZONTAL, (int) scorePanel.getMillisInGraph(), 0); // 3 hours
			long millisInGraph = scorePanel.getMillisInGraph();
			scoreTimeMillis.setValue((int)millisInGraph);
			scoreTimeMillis.addChangeListener(e -> scorePanelTime(e));
			scoreTimeMillis.setToolTipText("Minutes in graph "+((long) (millisInGraph/(1000*60))));
			add(scoreTimeMillis);
		}
		
		add(trainings);

		JPanel buttons = new JPanel();
		buttons.setLayout(new BoxLayout(buttons, BoxLayout.X_AXIS));
		
		pauseButton.addActionListener(e -> pauseToggle());
		buttons.add(pauseButton);

		interruptButton.addActionListener(e -> interruptTraining());
		buttons.add(interruptButton);
		
		saveButton.addActionListener(e -> saveNetwork());
		buttons.add(saveButton);
		saveButton.setEnabled(false);
	
		if(test != null) {
			testButton.addActionListener(e -> testNetwork());
			buttons.add(testButton);
			testButton.setEnabled(false);
		}
		
		add(buttons);
	}

	private void changeTrainingSetSizeSlider(ChangeEvent e) {
	    JSlider source = (JSlider) e.getSource();
	    if (!source.getValueIsAdjusting()) {
	        int sliderValue = (int)source.getValue();
	        int backpropperTrainingSetSize = (int) fromSliderToBackpropperTrainingSetSize(sliderValue);
	        backpropagator.setDefaultTrainingSetSize(backpropperTrainingSetSize);
	        trainingSetSizeSlider.setToolTipText("Training set size "+backpropperTrainingSetSize);
	    }
	}

	private void scorePanelTime(ChangeEvent e) {
	    JSlider source = (JSlider) e.getSource();
	    if (!source.getValueIsAdjusting()) {
	        long millisInGraph = (long)source.getValue();
			scorePanel.setMillisInGraph(millisInGraph);
			scoreTimeMillis.setToolTipText("Minutes in graph "+((long) (millisInGraph/(1000*60))));
	    }
	}

	private void testNetwork() {
		test.run();
	}

	private void saveNetwork() {
		backpropagator.getNeuralNetwork().persist();
	}

	private void interruptTraining() {
		backpropagator.setInterrupted(true);
	}

	private void pauseToggle() {
		boolean isPaused = backpropagator.isPaused();
		boolean shallBePauseState = !isPaused;
		if(shallBePauseState) {
			pauseButton.setText("Resume");
		}
		else {
			pauseButton.setText("Pause");
		}
		backpropagator.setPaused(shallBePauseState);
		saveButton.setEnabled(shallBePauseState);
		if(test != null) {
			testButton.setEnabled(shallBePauseState);
		}
	}

	private void changeLearningrate(ChangeEvent e) {
	    JSlider source = (JSlider)e.getSource();
	    if (!source.getValueIsAdjusting()) {
	        int sliderValue = (int) source.getValue();
	        double backpropperLearningrate = fromSliderToBackpropperLearningrate(sliderValue);
	        backpropagator.setLearningRate(backpropperLearningrate);
			learningrateSlider.setToolTipText("Learning rate "+backpropperLearningrate);
	    }
	}
	
	private void changeMomentum(ChangeEvent e) {
	    JSlider source = (JSlider)e.getSource();
	    if (!source.getValueIsAdjusting()) {
	        int sliderValue = (int)source.getValue();
	        double backpropperMomentum = fromSliderToBackpropperMomentum(sliderValue);
	        backpropagator.setMomentum(backpropperMomentum);
	        momentumSlider.setToolTipText("Momentum "+backpropperMomentum);
	    }
	}
	
	private void changeCharacteristicTime(ChangeEvent e) {
	    JSlider source = (JSlider)e.getSource();
	    if (!source.getValueIsAdjusting()) {
	        int sliderValue = (int)source.getValue();
	        double backpropperCharacteristicTime = fromSliderToBackpropperCharacteristicTime(sliderValue);
	        backpropagator.setCharacteristicTime(backpropperCharacteristicTime);
	        characteristicTimeSlider.setToolTipText("Characteristic time "+backpropperCharacteristicTime);
	    }
	}
	
	private void changeErrorThreshold(ChangeEvent e) {
	    JSlider source = (JSlider)e.getSource();
	    if (!source.getValueIsAdjusting()) {
	        int sliderValue = (int)source.getValue();
	        double backpropperErrorThreshold = fromSliderToBackpropperErrorThreshold(sliderValue);
	        backpropagator.setErrorThreshold(backpropperErrorThreshold);
	        errorThresholdSlider.setToolTipText("Error "+backpropperErrorThreshold);
	    }
	}
	
	public static final int learningratePowerShift = 4;
	public static final int learningratePowerWidth = 20;
	
	private static double fromSliderToBackpropperLearningrate(int slider) {
		return Math.pow(10, (((double) slider)/learningratePowerWidth)-learningratePowerShift);
	}
	
	private static int fromBackpropperToSliderLearningrate(double backpropperMomentum) {
		double outD = learningratePowerWidth*((Math.log(backpropperMomentum)/Math.log(10))+learningratePowerShift);
		return (int) outD;
	}

	public static final double momentumPowerShift = 4.5;
	public static final int momentumPowerWidth = 20;

	private static double fromSliderToBackpropperMomentum(int slider) {
		return Math.pow(10, (((double) slider)/momentumPowerWidth)-momentumPowerShift);
	}

	private static int fromBackpropperToSliderMomentum(double backpropperMomentum) {
		double outD = momentumPowerWidth*((Math.log(backpropperMomentum)/Math.log(10))+momentumPowerShift);
		return (int) outD;
	}
	
	public static final double trainingSetSizePowerShift = 0;
	public static final int trainingSetSizePowerWidth = 20;
	
	private double trainingDataSizeLog;

	private double fromSliderToBackpropperTrainingSetSize(int slider) {
		double fraction = ((double) slider)/100d;
		double exp = trainingDataSizeLog*fraction;
		return Math.exp(exp);
	}

	private int fromBackpropperToSliderTrainingSetSize(double backpropperTrainingSetSize) {
		double exp = Math.log(backpropperTrainingSetSize);
		double fraction = exp/trainingDataSizeLog;
		int slider = (int) (100*fraction);
		return slider;
	}
	
	public static final double errorThresholdPowerShift = 7.0;
	public static final int errorThresholdPowerWidth = 10;

	private static double fromSliderToBackpropperErrorThreshold(int slider) {
		return Math.pow(10, (((double) slider)/errorThresholdPowerWidth)-errorThresholdPowerShift);
	}

	private static int fromBackpropperToSliderErrorThreshold(double backpropperErrorThreshold) {
		double outD = errorThresholdPowerWidth*((Math.log(backpropperErrorThreshold)/Math.log(10))+errorThresholdPowerShift);
		return (int) outD;
	}
	
	public static final int characteristicTimePowerShift = 5;
	public static final int characteristicTimePowerWidth = 20;

	private static double fromSliderToBackpropperCharacteristicTime(int slider) {
		if(slider == 0) {
			return 0.0;
		}
		return Math.pow(10, (((double) slider)/characteristicTimePowerWidth)-characteristicTimePowerShift);
	}

	private static int fromBackpropperToSliderCharacteristicTime(double backpropperCharacteristicTime) {
		if(backpropperCharacteristicTime == 0) {
			return 0;
		}
		double outD = characteristicTimePowerWidth*((Math.log(backpropperCharacteristicTime)/Math.log(10))+characteristicTimePowerShift);
		return (int) outD;
	}
	
	public void updateData() {
		trainings.setText("Trainings "+backpropagator.getTrainingsDone());
	}

	public static void main(String args[]) {
		System.out.println("Mom 1 m->b "+fromSliderToBackpropperMomentum(0));
		System.out.println("Mom 0.00001 b->m "+fromBackpropperToSliderMomentum(0.00001));
		System.out.println("Mom 0 m->b->m "+fromBackpropperToSliderMomentum(fromSliderToBackpropperMomentum(0)));
		System.out.println("Mom 20 m->b->m "+fromBackpropperToSliderMomentum(fromSliderToBackpropperMomentum(20)));		System.out.println("LRa 20 "+fromBackpropperToSliderLearningrate(fromSliderToBackpropperLearningrate(20)));
	}

}
