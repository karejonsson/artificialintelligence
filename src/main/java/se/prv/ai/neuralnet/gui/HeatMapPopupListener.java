package se.prv.ai.neuralnet.gui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.statistics.HistogramType;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import general.reuse.swingutils.illustration.HeatMap;
import se.prv.ai.neuralnet.core.Synapse;

public class HeatMapPopupListener extends MouseAdapter {

	private HeatMap hm;
	private HeatGivingSynapseFactory sf;
	private int xspots;
	private int yspots;

	public HeatMapPopupListener(HeatMap hm, HeatGivingSynapseFactory sf) {
		this.hm = hm;
		this.sf = sf;
		xspots = hm.getXSpots();
		yspots = hm.getYSpots();
	}

	public void mousePressed(MouseEvent e) {
		maybeShowTextPopup(e);
	}

	public void mouseReleased(MouseEvent e) {
		maybeShowTextPopup(e);
	}

	private void maybeShowTextPopup(MouseEvent e) {
		//System.out.println("-----------------------");
		if(e.isPopupTrigger()) { 
			
			int currentWidth = hm.getWidth();
			double currentWidthPerHeatSpot = ((double) currentWidth) / xspots; 
			
			int currentHeight = hm.getHeight();
			double currentHeightPerHeatSpot = ((double) currentHeight) / yspots; 
			
			//System.out.println("POS X "+e.getX()+", Y "+e.getY());
			//System.out.println("Grid dimension W "+xspots+", H "+yspots);
			//System.out.println("Graph dimension W "+currentWidth+", H "+currentHeight);
			//System.out.println("Per spot W "+currentWidthPerHeatSpot+", H "+currentHeightPerHeatSpot);
			
			int currentHeatSpotW = (int) (e.getX()/currentWidthPerHeatSpot); 
			int currentHeatSpotH = (int) (e.getY()/currentHeightPerHeatSpot); 

			//System.out.println("In grid W "+currentHeatSpotW+", H "+currentHeatSpotH);

	        JPopupMenu popup = new JPopupMenu();
	        JMenuItem textevolution = new JMenuItem("Textual value evolution ("+currentHeatSpotW+", "+currentHeatSpotH+")");
	        popup.add(textevolution);
	        textevolution.addActionListener(ev -> showTextHistory(currentHeatSpotW, currentHeatSpotH));
	        JMenuItem graphicEvolutionSynapse = new JMenuItem("Value evolution ("+currentHeatSpotW+", "+currentHeatSpotH+")");
	        popup.add(graphicEvolutionSynapse);
	        graphicEvolutionSynapse.addActionListener(ev -> showGraphicalHistoryHeatSpot(currentHeatSpotW, currentHeatSpotH));
	        JMenuItem graphicEvolutionMatrix = new JMenuItem("Value standard deviation distribution matrix");
	        popup.add(graphicEvolutionMatrix);
	        graphicEvolutionMatrix.addActionListener(ev -> showGraphicalHistoryMatrix());
	        JMenuItem meanDistributionMatrix = new JMenuItem("Value mean distribution matrix");
	        popup.add(meanDistributionMatrix);
	        meanDistributionMatrix.addActionListener(ev -> showMeanDistributionMatrix());
	        
	        popup.show(e.getComponent(), e.getX(), e.getY());
		}      
	}
	
	private void showTextHistory(int currentHeatSpotW, int currentHeatSpotH) {
		Synapse synapse = sf.getSynapse(currentHeatSpotH, currentHeatSpotW);
		JOptionPane.showMessageDialog(null,
				makeTextContent(synapse),
			    "Analysis cell ("+currentHeatSpotW+", "+currentHeatSpotH+"), layer "+sf.getName(),
			    JOptionPane.INFORMATION_MESSAGE,
			    null);
	}
	
	private static String makeTextContent(Synapse synapse) {
		double[] history = synapse.getHistory();
		StringBuffer out = new StringBuffer();
		out.append(""+history[0]+"\n");
		for(int i = 1 ; i < history.length ; i++) {
			out.append("                    "+(history[i]-history[i-1])+"\n");
			out.append(""+history[i]+"\n");
		}

		SynapseStats stats = getStats(synapse);
		
		out.append("\nMean "+stats.mean+",  Stddev "+stats.stddev+"\n");

		return out.toString();
	}
	
	private void showGraphicalHistoryMatrix() {
		JOptionPane.showMessageDialog(null,
				standardDeviationDistributionHistogram(sf),
			    "Standard deviation distribution histogram for layer "+sf.getName(),
			    JOptionPane.INFORMATION_MESSAGE,
			    null);
	}

	private Object standardDeviationDistributionHistogram(HeatGivingSynapseFactory sf) {
		double[] listIn = new double[sf.getMaxY()*sf.getMaxX()];
		int pos = 0;

		for(int y = 0 ; y < sf.getMaxY() ; y++) {
			for(int x = 0 ; x < sf.getMaxX() ; x++) {
				Synapse synapse = sf.getSynapse(x, y);
				SynapseStats ss = getStats(synapse);
				listIn[pos++] = ss.stddev;
			}
		}

		// Création des datasets
		HistogramDataset dataset = new HistogramDataset();
		dataset.setType(HistogramType.RELATIVE_FREQUENCY);
		dataset.addSeries("Standard deviation distribution for synapses", listIn, 200);

		// Création de l'histogramme
		JFreeChart chart = ChartFactory.createHistogram("", null, null, dataset,
				PlotOrientation.VERTICAL, true, true, false);
		return new ChartPanel(chart);
	}

	private void showMeanDistributionMatrix() {
		JOptionPane.showMessageDialog(null,
				meanValueDistributionHistogram(sf),
			    "Mean value distribution histogram for layer "+sf.getName(),
			    JOptionPane.INFORMATION_MESSAGE,
			    null);
	}

	private Object meanValueDistributionHistogram(HeatGivingSynapseFactory sf) {
		double[] listIn = new double[sf.getMaxY()*sf.getMaxX()];
		int pos = 0;

		for(int y = 0 ; y < sf.getMaxY() ; y++) {
			for(int x = 0 ; x < sf.getMaxX() ; x++) {
				Synapse synapse = sf.getSynapse(x, y);
				SynapseStats ss = getStats(synapse);
				listIn[pos++] = ss.mean;
			}
		}

		// Création des datasets
		HistogramDataset dataset = new HistogramDataset();
		dataset.setType(HistogramType.RELATIVE_FREQUENCY);
		dataset.addSeries("Mean value distribution for synapses", listIn, 200);

		// Création de l'histogramme
		JFreeChart chart = ChartFactory.createHistogram("", null, null, dataset,
				PlotOrientation.VERTICAL, true, true, false);
		return new ChartPanel(chart);
	}

	private void showGraphicalHistoryHeatSpot(int currentHeatSpotW, int currentHeatSpotH) {
		Synapse synapse = sf.getSynapse(currentHeatSpotH, currentHeatSpotW);
		JOptionPane.showMessageDialog(null,
				makePanelContent(synapse.getHistory()),
			    "Analysis cell ("+currentHeatSpotW+", "+currentHeatSpotH+"), layer "+sf.getName(),
			    JOptionPane.INFORMATION_MESSAGE,
			    null);
	}
	
	public static SynapseStats getStats(Synapse synapse) {
		double[] history = synapse.getHistory();
		double sum = history[0];
		for(int i = 1 ; i < history.length ; i++) {
			sum += history[i];
		}
		double mean = sum/history.length;
		
		double sumSquareDeviation = 0.0;
		
        for(double num : history) {
        	sumSquareDeviation += Math.pow(num - mean, 2);
        }

        double stddev = Math.sqrt(sumSquareDeviation/history.length);
        return new SynapseStats(mean, stddev);
	}
	
	public static class SynapseStats {
		public double mean;
		public double stddev;
		public SynapseStats(double mean, double stddev) {
			this.mean = mean;
			this.stddev = stddev;
		}
	}
	
	private static JPanel makePanelContent(double[] history) {
		final XYSeries weights = new XYSeries("Weight");
        for(int i = 0 ; i < history.length ; i++) {
        	weights.add(i, history[i]);
        }
        final XYSeriesCollection data = new XYSeriesCollection();
        data.addSeries(weights);

        final JFreeChart chart = ChartFactory.createXYLineChart(
                "Evolution of weigth",
                "", // Below chart
                "Weight", 
                data,
                PlotOrientation.VERTICAL,
                true,
                true,
                false
            );

        chart.getXYPlot().getDomainAxis().setAutoRange(true);
 
		return new ChartPanel(chart);
	}
	
}
